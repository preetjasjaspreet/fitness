from django.db import models

class student(models.Model):
    name = models.CharField(max_length=100)
    roll_no = models.IntegerField(unique=True)
    email = models.EmailField(max_length=250)
    website = models.URLField(blank=True)
    dob = models.DateField()
    address = models.TextField(default='Chandigarh')
    branch = models.CharField(max_length=100,null=True)

    def __str__(self):
        return self.name
        # return str(self.roll_no)

class order(models.Model):
    custmor_id = models.IntegerField()
    amount = models.DecimalField(max_digits=10,decimal_places=2)
    received_on = models.DateTimeField(auto_now_add=True)

class account(models.Model):
    student_name = models.ForeignKey(student,on_delete=models.CASCADE)
    total_fee = models.DecimalField(max_digits=10,decimal_places=2)
    pending_fee = models.DecimalField(max_digits=10,decimal_places=2)
    added_on = models.DateTimeField(auto_now_add=True)    
def __str__(self):
        return self.student_name.email

class contact(models.Model):
    name=models.CharField(max_length=200)
    email = models.EmailField(max_length=250)
    message=models.CharField(max_length=200)
    feedback_date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
            return self.name

class employee(models.Model):
    SUB= (('Miss','Miss'),('Mr.','Mr.'),('Mrs.','Mrs.'))
    subtitle=models.CharField (choices=SUB,max_length=100)
    name=models.CharField(max_length=200)
    email = models.EmailField(max_length=250)
    ref_no= models.IntegerField()
    salary=models.DecimalField(max_digits=10,decimal_places=2)
    date_of_birth=models.DateField()
    date_of_joining=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name