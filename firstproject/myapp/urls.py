from django.urls import path
from myapp import views

app_name = 'myapp'

urlpatterns = [
    path('dashboard/',views.about,name='dashboard'),
    path('about/',views.about,name='ab'),
    path('contact/',views.contactView,name='con'),
    path('studentform/',views.empView,name='studentForm'),
    path('empform/',views.empView,name='employeeForm'),
    path('uslogin/',views.uslogin,name='uslogin'),
    path('rgUsingMF/',views.rgUsingMF,name='rgUsingMF'),
    path('usregister/',views.usregister,name='usregister'), 
    path('uslogout/',views.uslogout,name='logout'), 
    path('compdash/',views.compdash,name='compdash'),
    path('change_password/',views.change_password,name='comchange_password'),
    path('sendmail/',views.sendmail,name='sendmail'),

]