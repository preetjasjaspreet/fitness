from django.shortcuts import render
from mysite.models import category,product,ProductImage,review,brands,customer,cart,order,mail
from django.contrib.auth.models import User
import math,datetime
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.contrib import messages

#categories
cat = category.objects.filter(sub_cat=None)

all_products = product.objects.all().order_by('-id')
recent = all_products[:6]

brand = brands.objects.all().order_by('-id')[:3]
def all_cats():
    #All Categories with sub categories
    main=[]
    for i in cat:
        ab = []
        ab.append(i.category_name)
        ab += list(category.objects.filter(sub_cat=i).values())
        main.append(ab) 
    return main

def max_discount():
    total = 0
    sale = 0
    each = []
    
    for i in all_products:
        discount = (math.ceil(100-(i.sale_price/i.price*100)))
        ep={'id':i.id,'d':discount}
        each.append(ep)
        total+=i.price
        sale+=i.sale_price
    offer = 0
    pid = 0
    for m in each:
        if m['d'] > offer:
            offer = m['d']
            pid = m['id']
    getproduct = product.objects.get(id=pid)
    total_offer=math.ceil(100-(sale/total*100))
    final = {
        'p_id':getproduct.id,'p_name':getproduct.product_name,'p_image':getproduct.photo,'p_offer':offer,'t_offer':total_offer,'p_desc':getproduct.description.split('@'),'p_cat':getproduct.product_category.category_name,'p_brand':getproduct.brand,'p_price':getproduct.price,'sale_price':getproduct.sale_price,
    }
    return final
## Related Products
def related(filcat):
    searched_cat2 = category.objects.get(category_name=filcat)
    dt = category.objects.filter(sub_cat=searched_cat2).values()
    ab = []
    for i in dt:
        sub_cat = category.objects.get(id=i['id'])
        ab += list(product.objects.filter(product_category=sub_cat).values())
    rlproducts = product.objects.filter(product_category=searched_cat2).values()
    allproducts = list(rlproducts)
    final = ab+allproducts
    return final
def index(request):
    return render(request,'index.html',{'allcat':all_cats(),'max':max_discount(),'recent':recent,'brand':brand})

#products page
def showproducts(request):
    products= product.objects.all().order_by('price')
    main=[]
    for i in cat:
        ab = []
        ab.append(i.category_name)
        ab.append(i.id)
        ab += list(category.objects.filter(sub_cat=i).values())
        main.append(ab)

    # Filter by category
    if 'searchcat' in request.GET or 'scat' in request.GET:
        filcat = request.GET['searchcat']
        scat = request.GET['scat']
        
        searched_cat = category.objects.get(category_name=scat)
        products= product.objects.filter(product_category=searched_cat).order_by('-id')
             
        return render(request,'products.html',{'allcat':main,'p':products,'rp':related(filcat),'total':len(products),'cat':cat,'recent3':recent[:3],'max':max_discount() ,'recent':recent,'brand':brand})
    if 'term' in request.GET:
        t = request.GET['term']
        products=product.objects.filter(product_category__category_name__contains=t).order_by('-id')|product.objects.filter(product_name__contains=t).order_by('-id')|product.objects.filter(brand__brand_name__contains=t).order_by('-id')
    return render(request,'products.html',{'allcat':main,'p':products,'total':len(products),'cat':cat,'max':max_discount(),'recent':recent,'brand':brand,'recent':recent[:3]})

##Login
def uslogin(request):
    if "id" in request.COOKIES:
        id = request.COOKIES['id']
        user= User.objects.get(pk=id)
        login(request,user)
        return HttpResponseRedirect('/mysite/cart')
    if request.method=='POST':
        if 'signin' in request.POST:
            em = request.POST['email']
            pas = request.POST['password']
            user = authenticate(username=em,password=pas)
            if user:
                if user.is_active:
                    login(request,user)
                    request.session['id']=em
                    request.session['type']='customer'
                    response = HttpResponseRedirect('/mysite/cart')
                    if "remember" in request.POST:
                        response.set_cookie('id',user.id)
                        response.set_cookie('last_connection', datetime.datetime.now())
                        return response
                    else:
                        return response                                 
            else:
                response = 'Sorry, You Are Not Registered User'
                return render(request,'login.html',{'response':response,'allcat':all_cats(),'recent':recent,'brand':brand})
    return render(request,'login.html',{'allcat':all_cats(),'recent':recent,'brand':brand})

def usregister(request):
    if request.method=="POST":
        sb = request.POST['subtitle']
        fs = request.POST['first']
        ls = request.POST['last']
        ct = request.POST['city']
        em = request.POST['email']
        ps = request.POST['password']
        cps = request.POST['cpass']

        if(ps!=cps):
            return render(request,'register.html',{'status':'Password Does Not Match','allcat':all_cats(),'recent':recent,'brand':brand})

        else:
            user = User.objects.create_user(em,em,ps)
            user.first_name = fs
            user.last_name = ls
            user.save()

            profile = customer(user=user,subtitle=sb,address=ct)
            if 'profile' in request.FILES:
                pf = request.FILES['profile']
                profile.profile_pic=pf
            profile.save()
            return render(request,'register.html',{'status':'Thanks for register with Us','allcat':all_cats(),'recent':recent,'brand':brand})

    return render(request,'register.html',{'allcat':all_cats(),'recent':recent,'brand':brand})

@login_required
def dashboard(request):
    usr = customer.objects.get(user__id=request.user.id)
    return render(request,'dashboard.html',{'pp':usr,'allcat':all_cats(),'recent':recent,'brand':brand})

@login_required
def uslogout(request):
    logout(request)
    response = HttpResponseRedirect('/')
    response.delete_cookie('username')
    response.delete_cookie('id')
    return response

def addcart(request):
    if 'id' in request.session.keys():
        custo = customer.objects.get(user__username=request.user.username)
        cust = User.objects.get(username=request.session['id'])
        viewCart = cart.objects.filter(user_id=cust,type=0)
        total_cart = []
        if request.method == 'POST':
            item = request.POST['item_id']
            price = request.POST['amount']
            qty = request.POST['add']

            check = cart.objects.filter(type=0)
            product_object = product.objects.get(id=item)
            exist = False
            for x in check:
                if x.product_id==product_object and x.user_id==cust:
                    exist=True          
            if exist is True:  
                messages.warning(request,'Item Already in Your Cart')
            else:
                saveCart = cart(user_id=cust, product_id=product_object, quantity=qty)
                saveCart.save()
                messages.success(request,'Item Added to Your Cart')
        cid = ''
        for x in viewCart:
            cid+=str(x.id)+'/'
            user_cart = {}
            user_cart['id'] = x.id
            user_cart['p_id'] = x.product_id.id
            user_cart['product_name'] = x.product_id.product_name
            user_cart['photo'] = x.product_id.photo
            user_cart['product_cat'] = x.product_id.product_category
            user_cart['quantity'] = x.quantity
            user_cart['price'] = x.product_id.sale_price
            user_cart['totalprice'] = x.product_id.sale_price*x.quantity
            user_cart['market']= x.product_id.price
            user_cart['brand'] = x.product_id.brand
            user_cart['offer'] = math.ceil(100-(x.product_id.sale_price/x.product_id.price)*100)
            total_cart.append(user_cart)
        if 'q' in request.GET:
            newQ = request.GET['q']
            cartId = request.GET['id']
            getCart = cart.objects.get(id=cartId)
            getCart.quantity = newQ            
            getCart.save() 
            details = {
                'quantity':getCart.quantity,
                'sale_price':getCart.product_id.sale_price,
                'price':getCart.product_id.price,
                'cart_size':len(viewCart),
                'cids':cid,
            }          
            return JsonResponse(details)

        
        return render(request,'checkout.html',{'cart':total_cart,'size':len(total_cart),'cust':custo,'allcat':all_cats(),'recent':recent,'brand':brand})
    else:
        return render(request,'checkout.html',{'login':'You Need To Login first!!!','allcat':all_cats(),'recent':recent,'brand':brand})

def removeCart(request):
    if 'id' in request.session.keys():  
        
        if request.method=='GET':
            id = request.GET['removeid']
            deleteObj = cart.objects.get(id=id)
            deleteObj.delete()

            #User Cart 
            cust = User.objects.get(username=request.session['id'])
            viewCart = cart.objects.filter(user_id=cust)
            cart_size = len(viewCart)
            data = {
                'msz':'Item Removed Successfully!!!',
                'cart_size':cart_size,
            }
            return JsonResponse(data)
    else:
        messages.info(request, 'You need to login first!!!')
        return render(request,'checkout.html',{'allcat':all_cats(),'recent':recent,'brand':brand})

def grandTotal(request):
    if 'id' in request.session.keys():
        cust = User.objects.get(username=request.session['id'])
        cc = customer.objects.get(user=cust)
        viewCart = cart.objects.filter(user_id=cust,type=0)
        grand_total = 0
        quantity = 0
        c=''
        for i in viewCart:
            grand_total = grand_total+(i.product_id.sale_price*i.quantity)
            quantity = quantity+i.quantity
            c+=str(i.id)+'/'
            
        data = {
            'grand_total':grand_total,
            'quantity':quantity,
            'email':cust.username,
            'name':cust.first_name,
            'address':cc.address,
            'cids':c,
        }

        return JsonResponse(data)

def get_order(request):
    if 'id' in request.session.keys():
        us = User.objects.get(username=request.session['id'])
        cust = customer.objects.get(user=us)
        if request.method=='GET':
            dnm = request.GET['dname']
            dem = request.GET['demail']
            dmn = request.GET['dnumber']
            addr = request.GET['daddress']
            amt = request.GET['grandtotal']
            items = request.GET['items']

            order_obj = order(customer=cust,amount=amt,contact_name=dnm,contact_number=dmn,contact_email=dem,delivery_address=addr,cart_id=items)
            order_obj.save()
            oid = order_obj.id

            return HttpResponse(oid)

@login_required
def success_payment(request):
    if 'ORDERID' in request.GET:
        status = request.GET['STATUS']
        if status == "TXN_FAILURE":
            return HttpResponseRedirect('/mysite/cart')
        order_id = request.GET['ORDERID']
        txn_id = request.GET['TXNID']
        pay_mode = request.GET['PAYMENTMODE']
        bank_name = request.GET['BANKNAME']

        sid = order_id.split('o')[0]
        order_obj = order.objects.get(id=int(sid))
        order_obj.txn_id = txn_id
        order_obj.payment_mode= pay_mode
        order_obj.bank_name = bank_name
        order_obj.status = "Pending"
        order_obj.save()

        try:
            all_items = order_obj.cart_id
            if all_items!='' or all_items !=None:
                ls = all_items.split('/')
                for cid in ls[:-1]:
                    citem = cart.objects.get(id=cid)
                    citem.type=1
                    citem.save()
        except:
            return render(request,'process.html',{'status':'Something went wrong!!!','allcat':all_cats(),'cat':cat})

        return render(request,'process.html',{'txn_id':txn_id,'allcat':all_cats(),'cat':cat,'order':order_obj})

def single(request):
    products= product.objects.all().order_by('price')
    main=[]
    for i in cat:
        ab = []
        ab.append(i.category_name)
        ab.append(i.id)
        ab += list(category.objects.filter(sub_cat=i).values())
        main.append(ab)

    if 'id' in request.GET:
        id=request.GET['id']
        pd= product.objects.get(id=id)
        imgs=ProductImage.objects.get(product=pd)
        reviews=review.objects.filter(product=pd)

        data ={
            'pid':pd.id,
            'pname':pd.product_name,
            'pcat':pd.product_category,
            'pprice':pd.price,
            'sprice':pd.sale_price,
            'offer':round(100-(pd.sale_price/pd.price*100)),
            'pic':pd.photo,
            'brand':pd.brand,
            'des':pd.description.split('@'),
            'img1':imgs.image1,
            'img2':imgs.image2,
            'img3':imgs.image3,
            'img4':imgs.image4,

        }
        rel = related(data['pcat'])
    return render(request,'single.html',{'d':data,'allcat':main,'max':max_discount(),'rv':reviews,'trv':len(reviews),'rel':rel,'recent':recent,'brand':brand})

def add_review(request):
    if request.method=="POST":
        n=request.POST['name']
        id=request.POST['pid']
        e=request.POST['email']
        m=request.POST['msz']
        f=request.FILES['profile']

        pt = product.objects.get(id=id)
        rv = review(product=pt,name=n,email=e,profile=f,msz=m)
        rv.save()
        return render(request,'index.html',{'allcat':all_cats(),'max':max_discount(),'recent':recent,'status':'Review Added Successfully!! Thanks for your time!!!','recent':recent,'brand':brand})

def order_summary(request,id):
    return HttpResponse('Hello')

## get user
def getloginuser(id):
    ruser = User.objects.get(username=id)
    cust = customer.objects.get(user=ruser)
    details = {
        'id':ruser.id,
        'first_name':ruser.first_name,
        'last_name':ruser.last_name,
        'letter':ruser.first_name[0],
        'username':ruser.username,
        'city':cust.address,
        'subtitle':cust.subtitle,
        'custid':cust.id,
        'pic':cust.profile_pic,
        'registered_on':cust.registered_on,
    }
    return details

@login_required
def profile(request):
    details = getloginuser(request.user.username)
    return render(request,'profile.html',{'pp':details,'allcat':all_cats(),'recent':recent,'brand':brand})

@login_required
def change_profile(request):
    details = getloginuser(request.user.username)
    if request.method == 'POST':
        sub = request.POST['subtitle']
        first_name = request.POST['name']
        last_name = request.POST['lname']
        email = request.POST['email']
        address = request.POST['address']
        userid = request.POST['id']
        custid = request.POST['custid']

        userObj = User.objects.get(id=userid)
        if first_name != '':
            userObj.first_name = first_name
        if last_name != '':
            userObj.last_name = last_name
        if email != '': 
            userObj.email = email
        userObj.save()

        customerObj = customer.objects.get(id=custid)
        
        if address != '':
            customerObj.address = address
        
        if sub != '':
            customerObj.subtitle = sub
        if 'pic' in request.FILES:
            pic = request.FILES['pic']
            customerObj.profile_pic=pic    
        customerObj.save()
        return render(request,'change_profile.html',{'pp':details,'status':'Changes saved successfully!!!','all':all,'allcat':all_cats(),'recent':recent,'brand':brand})     
        
    return render(request,'change_profile.html',{'pp':details,'allcat':all_cats(),'recent':recent,'brand':brand})

def change_password(request):
    if request.method == 'POST':
        uid = request.POST['id']
        old = request.POST['oldpass']
        npass = request.POST['newpass']

        user = User.objects.get(id=uid)
        if user.check_password(old):
            user.set_password(npass)
            user.save()
            messages.success(request,'Password Changed!!!')
            return render(request,'index.html',{'status':'Password Changed!!! You Need To Login Again','allcat':all_cats(),'recent':recent,'brand':brand})
            
        else:
            messages.error(request,'Incorrect Current Password')
    return render(request,'change_password.html',{'pp':getloginuser(request.user.username),'allcat':all_cats(),'recent':recent,'brand':brand})

def order_history(request):
    us = User.objects.get(username=request.user.username)
    cust = customer.objects.get(user=us)
    orders = order.objects.filter(customer=cust).order_by('-id')
    all_data=[]
    for i in orders:
            ordrs = {
                    'id':i.id,
                    'txn_id':i.txn_id,
                    'payment_mode':i.payment_mode,
                    'received_on':i.received_on,
                    'delivered_at':i.delivery_address,
                    'amount':i.amount,
                    'status':i.status,
            }
            arr=[]
            for j in i.cart_id.split('/')[:-1]:
                    items = {}
                    data = cart.objects.get(id=j,type=1)
                    items.update({'name':data.product_id.product_name,'price':data.product_id.price,'photo':data.product_id.photo,'id':data.product_id.id,'quantity':data.quantity})
                    arr.append(items)
            ordrs.update({'items':arr})
            all_data.append(ordrs)
    return render(request,'orders.html',{'orders':all_data,'total':len(orders),'all':all,'allcat':all_cats(),'recent':recent,'brand':brand})

def about(request):
    return render(request,'about.html',{'allcat':all_cats(),'recent':recent,'brand':brand})

def contact(request):
    if request.method=="POST":
        name = request.POST['name']
        em = request.POST['email']
        sub = request.POST['subject']
        msz = request.POST['message']

        data = mail(name=name,email=em,subject=sub,message=msz)
        data.save()
        res='Thanks for your feedback, It is really means to us!!'
        return render(request,'mail.html',{'status':res,'allcat':all_cats(),'recent':recent,'brand':brand})    
    return render(request,'mail.html',{'allcat':all_cats(),'recent':recent,'brand':brand})

def uservalid(request):
    if request.method=='GET':
        if 'username' in request.GET:
            un = request.GET['username']
            check = User.objects.filter(username=un)
            if(len(check)>0):
                return HttpResponse('A user with this name already exists')
            else:
                return HttpResponse('Success')