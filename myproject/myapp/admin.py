from django.contrib import admin
from myapp.models import student,order,account,contact,employee,register
# Register your models here.
admin.site.site_header='My Project'

admin.site.register(student)
admin.site.register(order)
admin.site.register(account)
admin.site.register(contact)
admin.site.register(employee)
admin.site.register(register)