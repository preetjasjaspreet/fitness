from django.urls import path
from myapp import views

app_name = 'myapp'

urlpatterns = [
    # path('dashboard/',views.about,name='dashboard'),
 
    path('contactView/',views.contactView,name='contactView'),
    path('login/',views.login,name='login'),
    path('about/',views.about,name='about'),
    # path('studentform/',views.empView,name='studentForm'),
    # path('empform/',views.empView,name='employeeForm'),
    path('uslogin/',views.uslogin,name='uslogin'),
    # path('rgUsingMF/',views.rgUsingMF,name='rgUsingMF'),
    path('register/',views.registerView,name='register'),
    path('check_user',views.check_user,name='check_user'),
    # path('uslogout/',views.uslogout,name='logout'), 
    # path('compdash/',views.compdash,name='compdash'),
    # path('change_password/',views.change_password,name='comchange_password'),
    
]