from django.shortcuts import render
from myapp.models import student,contact,register
from myapp.forms import student,empForm,registerform
from django.http import HttpResponseRedirect
from django.contrib.auth import login,authenticate,logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
import datetime
# def index1(request):
#     if 'id' in request.COOKIES:
#         id=request.COOKIES['id']
#         user=check.objects.get(id=id)
#         login(request,check)
#         return HttpResponseRedirect('/myapp/dashboard')
#     return render(request,'index1.html')

# def index(request):
#     return HttpResponse('<h1>Welcome To Django</h1>')

# def first(request):
#     a = 'I am from views.py'
#     arr = ['red','green','blue','orange']
#     context = {'data':a,'color':arr}
#     return render(request,'index.html',context)

# def task(request):
#     users = [
#         {'name':'Aman','r_no':1},
#         {'name':'abcd','r_no':2},
#         {'name':'peter','r_no':6},
#     ]
#     colors = ['red','green','blue','orange','#000']
#     ## Create task.html
#     return render(request,'task.html',{'c':colors,'u':users})

# def homepage(request):
#     return render(request,'home.html')

# def users(request):
#     data = student.objects.all()
#     data = student.objects.all().order_by('-id')
#     # data = student.objects.all().order_by('name')
#     return render(request,'users.html',{'users':data,'total':len(data)})

def base(request):
    return render(request,'base.html')

def index(request):
    return render(request,'index.html')

def uslogin(request):
    return render(request,'login.html') 

def contact(request):
    return render(request,'contact.html')

def about(request):
    return render(request,'about.html')
      
def contactView(request):
    if request.method=='POST':
        print(request.POST)
        n=request.POST['name']
        e=request.POST['email']
        m=request.POST['massage']
        data=contact(name=n,email=e,message=m)
        data.save()
        res='hello{}... Thanks for your feedback'.format(n)
        return render(request,'contact.html',{'result':res})
        print('data saved successfully!!')
    return render(request,'contact.html')
    
# def studentForm(request):
#     form = Student()
#     return render(request,'studentForm.html',{'form':form})

# def empView(request):
#     myform = empForm()
#     if request.method=='POST':
#         myform = empForm(request.POST)
#         if myform.is_valid():
#             myform.save()
#             context={'frm':myform,'color':'success','msz':'Saved Successfully!!'}
#             return render(request,'employee.html',context)
#     return render(request,'employee.html',{'frm':myform})


def registerView(request):
    if request.method=="POST":
        n= request.POST['name']
        c= request.POST['contact']
        em = request.POST['email']        
        pwd = request.POST['password']
                
        user = User.objects.create_user(em,em,pwd)
        user.first_name =n
        user.is_staff=True
        user.save()
        
        profile = register(name=user,contact=c)
        profile.save()
        return render(request,'register.html',{'status':'Success'})
    return render(request,'register.html')

def check_user(request):
    un = request.GET['username']
    check = User.objects.filter(username=un)
    if len(check)!=0:
        return HttpResponse("A user with this name already exists!!")
    else:
        return HttpResponse("Username Validation Success!!!")

# def rgUsingMF(request):
#     form = registerform()
#     if request.method=='POST':
#         form = registerform(data=request.POST)
#         if form.is_valid():
#             user=form.save()
#             user.set_password(user.password)
#             form.save()
#             return render(request,'register.html',{'frm':form,'status':'Registred Successfully!!!'})

#     return render(request,'register.html',{'frm':form}) 
def uslogin(request):  
    if request.method=='POST':
        usn = request.POST['username']
        pwd = request.POST['password']
        user = authenticate(username=usn,password=pwd)
    
        if user:
            if user.is_staff:
                login(request,user)
                response = HttpResponseRedirect('/myapp/about')
                response.set_cookie('username',usn)
                response.set_cookie('id',user.id)
                response.set_cookie('logintime',datetime.datetime.now())
                return response
            elif user.is_active:
                login(request,user)
                response = HttpResponseRedirect('/myapp/about')
                response.set_cookie('username',usn)
                response.set_cookie('id',user.id)
                response.set_cookie('logintime',datetime.datetime.now())
                return response
            
                return response
        else:
            return render(request,'login.html',{'status':'Invalid Details'})

    return render(request,'login.html')

# def uslogout(request):
#     logout (request)
#     response= HttpResponseRedirect('/')
#     response.delete_cookie('id')
#     response.delete_cookie('username')
#     return response

# @login_required
# def usregister(request):
#     return render(request,'register.html')

# @login_required
# def compdash(request):
#     return render(request,'companydashbord.html')

# from django.contrib.auth.hashers import check_password


# @login_required
# def change_password(request):
#     login_user_password=request.user.password
#     if request.method == 'POST':
#         # print request.post
#         current = request.POST['old']
#         newpas = request.POST['new']

#         check=check_password(current,login_user_password)
#         if check==True:
#             user.set_password(newpas)
#             user.save()
#             dict ={'status':'password changed successfully!!','col':'success'}
#             return render()

#         else:
#             print('Does ')    
    
#     return render(request,'change_password.html')